"""
Here we are doing regression testing for the new bonding routine that operates and the mbuild initial structure
whose volume is shrunk to a density of 1.0
:param datadir:
:param tmpdir:
:return:
"""
from epoxpy.abc_type_epoxy_simulation import ABCTypeEpoxySimulation
import epoxpy.abc_type_epoxy_lj_harmonic_simulation as es
import epoxpy.temperature_profile_builder as tpb
import random
import os
import gsd.hoomd
import numpy as np
import epoxpy.common as cmn

random.seed(1020)

mix_time = 1e3
mix_kt = 2.0
cure_kt = 2.0
time_scale = 100
n_mul = 6.0
n_part = n_mul*50
type_A_md_temp_profile = tpb.LinearTemperatureProfileBuilder(initial_temperature=mix_kt, initial_time=mix_time)
type_A_md_temp_profile.add_state_point(500 * time_scale, cure_kt)
out_dir = str('')
sim_name = 'shrunk_freud_bonding'
out_dir = os.path.join(out_dir, sim_name)
myEpoxySim = es.ABCTypeEpoxyLJHarmonicSimulation(sim_name, mix_time=mix_time, mix_kt=mix_kt,
                                                 temp_prof=type_A_md_temp_profile,
                                                 bond=True, n_mul=n_mul, shrink=True,
                                                 shrink_time=1e4,
                                                 mix_dt=1e-4,
                                                 md_dt=1e-2,
                                                 integrator=cmn.Integrators.NPT.name,
                                                 output_dir=out_dir,
                                                 use_dybond_plugin=True,
                                                 density=0.01)

myEpoxySim.execute()

current_gsd = os.path.join(out_dir, 'data.gsd')
gsd_path = str(current_gsd)
#print('reading gsd: ', gsd_path)
f = gsd.fl.GSDFile(gsd_path, 'rb')
t = gsd.hoomd.HOOMDTrajectory(f)
snapshot = t[-1]
current_bonds = snapshot.bonds.N
assert snapshot.particles.N == n_part
#print('test_epoxy_sim_freud_shrunk_regression. current:{}'.format(current_bonds))
assert current_bonds >= 1  # Just checking if some bonds are being made

idxs, counts = np.unique(snapshot.bonds.group, return_counts=True)
#print('########################idxs',idxs,counts)
#print(snapshot.bonds.group)
for index,idx  in enumerate(idxs):
    p_typeid = snapshot.particles.typeid[idx]
    p_type = snapshot.particles.types[p_typeid]
    if p_type == 'A':
        assert (counts[index] <= myEpoxySim.max_a_bonds)
    elif p_type == 'B':
        assert (counts[index] <= myEpoxySim.max_b_bonds)

